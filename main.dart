import 'package:flutter/material.dart';
import 'dart:ui';

void main() {
  runApp(FancyHelloWidget());
}

class FancyHelloWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Boitumelo Moshitwa Viibes'),
        ),
        body: Container(
          alignment: Alignment.center,
          child: const Text('Welcome to Viibes'),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.thumb_up),
          onPressed: () => {},
        ),
      ),
    );
  }
}
